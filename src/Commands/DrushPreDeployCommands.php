<?php

namespace Drupal\drush_pre_deploy\Commands;

use Drupal\Core\Update\UpdateRegistry;
use Drupal\Core\Utility\Error;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\UserAbortException;
use Psr\Log\LogLevel;
use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;

/**
 * Pre-deploy drush command class.
 */
class DrushPreDeployCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  /**
   * Command successfully completed some operation.
   */
  const SUCCESS = 'success';

  /**
   * Update registry.
   *
   * @var \Drupal\Core\Update\UpdateRegistry
   */
  protected UpdateRegistry $registry;

  /**
   * {@inheritDoc}
   */
  public function __construct(string $root, string $site_path, ModuleHandlerInterface $moduleHandler, KeyValueFactoryInterface $keyValueFactory) {
    $this->registry = new class(
      $root,
      $site_path,
      array_keys($moduleHandler->getModuleList()),
      $keyValueFactory->get('pre_deploy_hook')
    ) extends UpdateRegistry {

      /**
       * Sets the update registry type.
       *
       * @param string $type
       *   The registry type.
       */
      public function setUpdateType($type) {
        $this->updateType = $type;
      }

    };
    $this->registry->setUpdateType('predeploy');
  }

  /**
   * Prints information about pending pre-deploy update hooks.
   *
   * @usage deploy:pre-hook-status
   *   Prints information about pending pre-deploy hooks.
   *
   * @field-labels
   *   module: Module
   *   hook: Hook
   *   description: Description
   * @default-fields module,hook,description
   *
   * @command deploy:pre-hook-status
   * @bootstrap full
   *
   * @filter-default-field hook
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields
   *   A list of pending hooks.
   *
   * @phpcs:disable Generic.CodeAnalysis.UselessOverridingMethod.Found
   */
  public function status(): RowsOfFields {
    $updates = $this->registry->getPendingUpdateInformation();
    $rows = [];
    foreach ($updates as $module => $update) {
      if (!empty($update['pending'])) {
        foreach ($update['pending'] as $hook => $description) {
          $rows[] = [
            'module' => $module,
            'hook' => $hook,
            'description' => $description,
          ];
        }
      }
    }

    return new RowsOfFields($rows);
  }

  /**
   * Runs pre-deploy hooks.
   *
   * @usage deploy:pre-hook
   *   Runs pending pre-deploy hooks.
   *
   * @command deploy:pre-hook
   * @bootstrap full
   *
   * @return int
   *   0 for success, 1 for failure.
   */
  public function run(): int {
    $pending = $this->registry->getPendingUpdateFunctions();

    if (empty($pending)) {
      $this->logger()->success(dt('No pending pre-deploy hooks.'));
      return self::EXIT_SUCCESS;
    }

    $process = $this->processManager()->drush($this->siteAliasManager()->getSelf(), 'deploy:pre-hook-status');
    $process->mustRun();
    $this->output()->writeln($process->getOutput());

    if (!$this->io()->confirm(dt('Do you wish to run the specified pending pre deploy hooks?'))) {
      throw new UserAbortException();
    }

    $success = TRUE;
    if (!$this->getConfig()->simulate()) {
      $success = $this->doRunPendingHooks($pending);
    }

    $level = $success ? self::SUCCESS : LogLevel::ERROR;
    $this->logger()->log($level, dt('Finished performing pre deploy hooks.'));
    return $success ? self::EXIT_SUCCESS : self::EXIT_FAILURE;
  }

  /**
   * Mark all pre-deploy hooks as having run.
   *
   * @hook pre-command deploy:mark-complete
   */
  public function markComplete(): int {
    $pending = $this->registry->getPendingUpdateFunctions();
    $this->registry->registerInvokedUpdates($pending);

    $this->logger()->success(dt('Marked %count pending pre-deploy hooks as complete.', ['%count' => count($pending)]));
    return self::EXIT_SUCCESS;
  }

  /**
   * Runs pending hooks.
   *
   * @param array $pending
   *   An array of hooks to execute.
   *
   * @return bool
   *   TRUE if everything ran correctly, FALSE otherwise.
   */
  protected function doRunPendingHooks(array $pending) {
    try {
      foreach ($pending as $function) {
        $func = new \ReflectionFunction($function);
        $this->logger()->notice('Predeploy hook started: ' . $func->getName());

        // Pretend it is a batch operation to keep the same signature
        // as the post update hooks.
        $sandbox = [];
        do {
          $return = $function($sandbox);
          if (!empty($return)) {
            $this->logger()->notice($return);
          }
        } while (isset($sandbox['#finished']) && $sandbox['#finished'] < 1);

        $this->registry->registerInvokedUpdates([$function]);
        $this->logger()->debug('Performed: ' . $func->getName());
      }

      return TRUE;
    }
    catch (\Throwable $e) {
      $variables = Error::decodeException($e);
      unset($variables['backtrace']);
      $this->logger()->error(dt('%type: @message in %function (line %line of %file).', $variables));
      return FALSE;
    }
  }

}

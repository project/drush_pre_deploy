<?php

namespace Drush;

use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Drush\Commands\DrushCommands;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Symfony\Component\Console\Exception\CommandNotFoundException;

/**
 * Provides a pre-command hook to drush deploy command.
 */
class DrushPreDeployHookCommands extends DrushCommands implements SiteAliasManagerAwareInterface {
  use SiteAliasManagerAwareTrait;

  /**
   * Provides a pre-command hook when deploy command is executed.
   *
   * @hook pre-command deploy
   */
  public function preDeployHook() {
    try {
      \Drush\Drush::getApplication()->find('deploy:pre-hook');
    }
    catch (CommandNotFoundException $e) {
      $this->logger()->warning("Drush pre-deploy: skipping external execution of drush deploy:pre-hook, the drush_pre_deploy module must be enabled beforehand.");
      return;
    }
    $self = $this->siteAliasManager()->getSelf();
    $redispatchOptions = Drush::redispatchOptions();
    $manager = $this->processManager();

    $this->logger()->notice("Drush pre-deploy hook start.");
    $process = $manager->drush($self, 'deploy:pre-hook', [], $redispatchOptions);
    $process->mustRun($process->showRealtime());
  }

}

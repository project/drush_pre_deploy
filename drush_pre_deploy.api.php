<?php

/**
 * @file
 * API documentation for Drush Pre Deploy module.
 */

/**
 * Execute code before a deployment.
 *
 * This hook can be used to execute code before a deployment.
 *
 * @param array $sandbox
 *    Stores information for batch updates.
 *
 * @return string|null
 *    Optionally, hook_predeploy_NAME() hooks may return a translated string
 *    that will be displayed to the user after the update has completed. If no
 *    message is returned, no message will be presented to the user.
 *
 * @throws \Exception
 *    In case of error, update hooks should throw an instance of
 *    \Exception with a meaningful message for the user.
 *
 * @ingroup hooks
 */
function hook_predeploy_NAME(array &$sandbox): ?string {
    $node = \Drupal\node\Entity\Node::load(123);
    $node->setTitle('foo');
    $node->save();

    return t('Node %nid saved', ['%nid' => $node->id()]);
}
